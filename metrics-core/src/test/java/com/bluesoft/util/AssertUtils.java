/*
 * Copyright 2014 Dana H. P'Simer & BluesSoft Development, LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.bluesoft.util;

import java.math.BigDecimal;
import static org.testng.Assert.*;

/**
 *
 * @author danap
 */
public class AssertUtils {

  public static void assertBigDecimalEquals( BigDecimal actual, BigDecimal expected ) {
    assertBigDecimalEquals( actual, expected, null );
  }

  public static void assertBigDecimalEquals( BigDecimal actual, BigDecimal expected, String message ) {
    assertTrue( BigDecimalUtils.equals( actual, expected ),
                (message == null ? "" : message + ": ") + "Expected [" + expected + "] but found [" + actual + "]" );
  }
}
