/*
 * Copyright 2014 Dana H. P'Simer & BluesSoft Development, LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.bluesoft.util;

import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;
import java.util.Iterator;
import java.util.Random;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import static com.bluesoft.util.AssertUtils.assertBigDecimalEquals;
import static org.testng.Assert.assertFalse;
import static org.testng.Assert.assertTrue;
import static org.testng.Assert.assertTrue;

/**
 *
 * @author danap
 */
public class BigDecimalUtilsTest {

  private final org.slf4j.Logger LOG = org.slf4j.LoggerFactory.getLogger( BigDecimalUtilsTest.class );

  private final MathContext mc = new MathContext( 10, RoundingMode.HALF_UP );
  private final MathContext highPrecMC = new MathContext( 100, RoundingMode.HALF_UP );
  private final Random rand = new Random();

  @DataProvider
  public Iterator<Object[]> roots() {
    return new Iterator<Object[]>() {
      BigDecimal[] testValues = { BigDecimal.ZERO, BigDecimal.valueOf( 7766666667L, 9 ), BigDecimal.valueOf( 2786873996L, 9 ), BigDecimal.valueOf(
        2786873996L, 9 ) };
      int stage = 0;
      int maxPrecision = 10;
      int maxSquares = 20;
      int precision = 0;
      int i = 1;
      int maxRandom = 1000;

      @Override
      public boolean hasNext() {
        if ( stage == 0 ) {
          return i < maxSquares && precision < maxPrecision;
        } else if ( stage == 1 ) {
          return i < maxRandom;
        } else if ( stage == 2 ) {
          return i < testValues.length;
        } else {
          throw new IllegalStateException( "Unrecognized state." );
        }
      }

      @Override
      public Object[] next() {
        BigDecimal v = null;
        if ( stage == 0 ) {
          v = BigDecimal.valueOf( i, precision );
        } else if ( stage == 1 ) {
          v = BigDecimal.valueOf( Math.abs( rand.nextLong() % 100000000L ), rand.nextInt( maxPrecision ) );
        } else if ( stage == 2 ) {
          v = testValues[i];
        } else {
          throw new IllegalStateException( "Unrecognized state." );
        }
        i += 1;
        if ( stage == 0 ) {
          if ( i >= maxSquares ) {
            i = 1;
            precision += 1;
            if ( precision >= maxPrecision ) {
              stage = 1;
            }
          }
        } else if ( stage == 1 ) {
          if ( i >= maxRandom ) {
            stage = 2;
            i = 0;
          }
        }
        return new Object[]{ v };
      }

      @Override
      public void remove() {
        throw new UnsupportedOperationException();
      }
    };
  }

  @Test( dataProvider = "roots" )
  public void testSqrt( BigDecimal ii ) {
    BigDecimal A = ii.pow( 2 );
    LOG.debug( "ii = {}, A = {}", ii, A );
    assertBigDecimalEquals( BigDecimalUtils.sqrt( A, mc ), ii );
  }

  @Test( dataProvider = "roots" )
  public void testSqrtHighPrecision( BigDecimal ii ) {
    BigDecimal A = ii.pow( 2 );
    LOG.debug( "ii = {}, A = {}", ii, A );
    assertBigDecimalEquals( BigDecimalUtils.sqrt( A, highPrecMC ), ii );
  }

  @Test( expectedExceptions = ArithmeticException.class )
  public void testSqrtNegative() {
    BigDecimalUtils.sqrt( BigDecimal.valueOf( -1.5 ), mc );
  }

  @Test( expectedExceptions = IllegalArgumentException.class )
  public void testZeroPrecision() {
    BigDecimalUtils.sqrt( BigDecimal.valueOf( 4.0 ), new MathContext( 0 ) );
  }

  @Test
  public void testEquals() {
    assertTrue( BigDecimalUtils.equals( BigDecimal.ONE, BigDecimal.ONE ) );
    assertTrue( BigDecimalUtils.equals( BigDecimal.valueOf( 5.0 ), BigDecimal.valueOf( 5.0 ) ) );
    assertFalse( BigDecimalUtils.equals( BigDecimal.ONE, BigDecimal.ZERO ) );
    assertFalse( BigDecimalUtils.equals( BigDecimal.ZERO, BigDecimal.ONE ) );
  }

  @Test( expectedExceptions = IllegalArgumentException.class )
  public void testNullEqualsRight() {
    BigDecimalUtils.equals( BigDecimal.ONE, null );
  }

  @Test( expectedExceptions = IllegalArgumentException.class )
  public void testNullEqualsLeft() {
    BigDecimalUtils.equals( BigDecimal.ONE, null );
  }
}
