/*
 * Copyright 2014 Dana H. P'Simer & BluesSoft Development, LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.bluesoft.util.metrics.core.impl;

import com.bluesoft.util.metrics.core.domain.BasicMetricEvent;
import com.bluesoft.util.metrics.core.domain.BasicTimingEvent;
import com.bluesoft.util.metrics.core.MeteringMetric;
import com.bluesoft.util.metrics.core.TimingMetric;
import java.math.BigInteger;
import java.util.concurrent.Executor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;

/**
 *
 * @author danap
 */
public class AsyncEventProcessingManagerTest {

  private static final Logger LOG = LoggerFactory.getLogger( AsyncEventProcessingManagerTest.class );

  private int eventCount;

  @Test
  public void testAddEventTimingMetric() {
    eventCount = 0;
    AsyncEventProcessingManager testManager = new AsyncEventProcessingManager( new Executor() {
      @Override
      public void execute( Runnable command ) {
        eventCount += 1;
        command.run();
      }
    } );

    TimingMetric baseTimingMetric = new BasicTimingMetric( "testTimingMetric" );
    TimingMetric asyncTimingMetric = testManager.createQueuedTimingMetric( baseTimingMetric );
    long now = System.currentTimeMillis();
    asyncTimingMetric.addEvent( new BasicTimingEvent( now - 5, now ) );
    assertEquals( baseTimingMetric.getCount(), BigInteger.ONE );
    assertEquals( asyncTimingMetric.getCount(), BigInteger.ONE );
    assertEquals( eventCount, 1 );
  }

  @Test
  public void testAddEventMeteringMetric() {
    eventCount = 0;
    AsyncEventProcessingManager testManager = new AsyncEventProcessingManager();
    testManager.setExecutor( new Executor() {
      @Override
      public void execute( Runnable command ) {
        eventCount += 1;
        command.run();
      }
    } );

    MeteringMetric baseMeteringMetric = new BasicMeteringMetric( "testMeteringMetric" );
    MeteringMetric asyncMeteringMetric = testManager.createQueuedMeteringMetric( baseMeteringMetric );
    asyncMeteringMetric.addEvent( new BasicMetricEvent() );
    assertEquals( baseMeteringMetric.getCount(), BigInteger.ONE );
    assertEquals( asyncMeteringMetric.getCount(), BigInteger.ONE );
    assertEquals( eventCount, 1 );
  }
}
