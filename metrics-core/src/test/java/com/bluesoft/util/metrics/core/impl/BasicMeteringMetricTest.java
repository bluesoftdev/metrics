/*
 * Copyright 2014 Dana H. P'Simer & BluesSoft Development, LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.bluesoft.util.metrics.core.impl;

import com.bluesoft.util.metrics.core.Clock;
import com.bluesoft.util.metrics.core.MeteringMetric;
import com.bluesoft.util.metrics.core.Rate;
import com.bluesoft.util.metrics.core.domain.BasicMetricEvent;
import java.math.BigDecimal;
import java.util.concurrent.TimeUnit;
import org.testng.annotations.Test;

import static org.easymock.EasyMock.createMock;
import static org.easymock.EasyMock.expect;
import static org.easymock.EasyMock.replay;
import static org.easymock.EasyMock.verify;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertNotNull;
import static org.testng.Assert.assertNull;
import static org.testng.Assert.assertSame;
import static org.testng.Assert.assertTrue;

/**
 *
 * @author danap
 */
public class BasicMeteringMetricTest {

  @Test
  public void testCtor() {
    BasicMeteringMetric child1 = new BasicMeteringMetric( "child1" );
    BasicMeteringMetric child2 = new BasicMeteringMetric( "child2" );
    BasicMeteringMetric mm = new BasicMeteringMetric( "test", child1, child2 );
    assertEquals( mm.getChildNames().size(), 2 );
    assertSame( mm.getChild( "child1" ), child1 );
    assertSame( mm.getChild( "child2" ), child2 );
  }

  @Test
  public void testMetering() throws Exception {
    Clock clock = createMock( Clock.class );
    expect( clock.getUnit() ).andStubReturn( TimeUnit.MILLISECONDS );
    expect( clock.getTime() ).andReturn( 0L ).once();
    expect( clock.getTime() ).andReturn( 10L ).once();
    expect( clock.getTime() ).andReturn( 100L ).once();
    expect( clock.getTime() ).andReturn( 1000L ).times( 2 );
    expect( clock.getTime() ).andReturn( 2000L ).times( 2 );
    expect( clock.getTime() ).andReturn( 4000L ).times( 2 );
    expect( clock.getTime() ).andReturn( 5000L ).times( 2 );
    expect( clock.getTime() ).andReturn( 6000L ).times( 2 );
    expect( clock.getTime() ).andReturn( 6500L ).times( 3 );

    replay( clock );
    BasicMeteringMetric mm = new BasicMeteringMetric( "foo" );
    mm.setClock( clock );

    assertNull( mm.getRate() );
    assertEquals( mm.getRateUnit(), TimeUnit.SECONDS );

    mm.addEvent( new BasicMetricEvent() );

    Rate r = mm.getRate();
    assertTrue( r.getValue().compareTo( BigDecimal.valueOf( 1L ) ) == 0, ": expected 1 but found " + r.getValue() );
    assertEquals( r.getUnit(), TimeUnit.SECONDS );

    mm.addEvent( new BasicMetricEvent() );
    mm.addEvent( new BasicMetricEvent() );
    mm.addEvent( new BasicMetricEvent() );
    mm.addEvent( new BasicMetricEvent() );

    r = mm.getRate();
    assertTrue( r.getValue().compareTo( BigDecimal.valueOf( 5L ) ) == 0, ": expected 5 but found " + r.getValue() );
    assertEquals( r.getUnit(), TimeUnit.SECONDS );

    MeteringMetric snapshot = (MeteringMetric)mm.getSnapshot();

    r = snapshot.getRate();
    assertTrue( r.getValue().compareTo( BigDecimal.valueOf( 5L ) ) == 0, ": expected 5 but found " + r.getValue() );
    assertEquals( r.getUnit(), TimeUnit.SECONDS );

    for ( int i = 0; i < 15; i++ ) {
      mm.addEvent( new BasicMetricEvent() );
    }

    snapshot = (MeteringMetric)mm.getSnapshot();

    r = snapshot.getRate();
    assertTrue( r.getValue().compareTo( BigDecimal.valueOf( 10L ) ) == 0, ": expected 10 but found " + r.getValue() );
    assertEquals( r.getUnit(), TimeUnit.SECONDS );

    snapshot = (MeteringMetric)mm.getSnapshot();

    r = snapshot.getRate();
    assertTrue( r.getValue().compareTo( BigDecimal.valueOf( 5L ) ) == 0, ": expected 5 but found " + r.getValue() );
    assertEquals( r.getUnit(), TimeUnit.SECONDS );

    snapshot = (MeteringMetric)mm.getSnapshot();

    r = snapshot.getRate();
    assertTrue( r.getValue().compareTo( BigDecimal.valueOf( 4L ) ) == 0, ": expected 4 but found " + r.getValue() );
    assertEquals( r.getUnit(), TimeUnit.SECONDS );

    snapshot = (MeteringMetric)mm.getSnapshot();

    r = snapshot.getRate();
    assertTrue( r.getValue().compareTo( BigDecimal.valueOf( 33333333333L, 10 ) ) == 0, ": expected 3.3333333333 but found " + r.getValue() );
    assertEquals( r.getUnit(), TimeUnit.SECONDS );

    snapshot = (MeteringMetric)mm.reset();

    assertNull( mm.getRate() );
    r = snapshot.getRate();
    assertNotNull( r );
    assertTrue( r.getValue().compareTo( BigDecimal.valueOf( 33333333333L, 10 ) ) == 0, ": expected 3.3333333333 but found " + r.getValue() );
    assertEquals( r.getUnit(), TimeUnit.SECONDS );
    verify( clock );
  }
}
