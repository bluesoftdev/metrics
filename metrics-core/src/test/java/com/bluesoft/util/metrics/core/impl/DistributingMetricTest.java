/*
 * Copyright 2014 Dana H. P'Simer & BluesSoft Development, LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.bluesoft.util.metrics.core.impl;

import com.bluesoft.util.metrics.core.Conditions;
import com.bluesoft.util.metrics.core.MeteringMetric;
import com.bluesoft.util.metrics.core.TimingMetric;
import com.bluesoft.util.metrics.core.domain.BasicTimingEvent;
import java.math.BigInteger;
import org.testng.annotations.Test;

import static com.bluesoft.endurance.util.collection.Maps.map;
import static com.bluesoft.endurance.util.collection.Pair.pairOf;
import static org.testng.Assert.assertEquals;

/**
 *
 * @author Dana P'Simer &lt;danap@bluesoftdev.com&gt;
 */
public class DistributingMetricTest {

  private static final org.slf4j.Logger LOG = org.slf4j.LoggerFactory.getLogger( DistributingMetricTest.class );

  @Test
  public void testDistrbution() {
    TimingMetric t = new BasicTimingMetric( "testTimer" );
    MeteringMetric m = new BasicMeteringMetric( "testMeter" );
    DistributingMetric dm = new DistributingMetric<>( "testDist",
                                                      map( pairOf( Conditions.all(), "testTimer" ), pairOf( Conditions.exception(), "testMeter" ) ),
                                                      t, m );

    dm.addEvent( new BasicTimingEvent( 0, 1000L ) );
    dm.addEvent( new BasicTimingEvent( 0, 2000L, null, null, new RuntimeException( "test ex" ) ) );

    assertEquals( t.getCount(), BigInteger.valueOf( 2 ) );
    assertEquals( m.getCount(), BigInteger.valueOf( 1 ) );
  }

}
