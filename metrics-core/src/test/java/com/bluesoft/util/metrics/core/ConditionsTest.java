/*
 * Copyright 2014 Dana H. P'Simer & BluesSoft Development, LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.bluesoft.util.metrics.core;

import com.bluesoft.util.metrics.core.domain.BasicTimingEvent;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;

/**
 *
 * @author Dana P'Simer &lt;danap@bluesoftdev.com&gt;
 */
public class ConditionsTest {

  private static final org.slf4j.Logger LOG = org.slf4j.LoggerFactory.getLogger( ConditionsTest.class );

  @DataProvider
  public Object[][] scenarios() {
    return new Object[][]{
      new Object[]{ new BasicTimingEvent<>( 0, 1000L, null, null, null ), true, false, false },
      new Object[]{ new BasicTimingEvent<>( 0, 1000L, new Object[]{ "foo", "bar" }, "snafu", null ), true, false, false },
      new Object[]{ new BasicTimingEvent<>( 0, 1000L, new Object[]{ "foo", "bar" }, "snafu", new IllegalStateException( "test ex" ) ), true, true,
        true },
      new Object[]{ new BasicTimingEvent<>( 0, 1000L, new Object[]{ "foo", "bar" }, "snafu", new IllegalArgumentException( "test ex" ) ), true, true,
        false },
      new Object[]{ new BasicTimingEvent<>( 0, 1000L, new Object[]{ "foo", "bar" }, "snafu", new UnsupportedOperationException( "test ex" ) ), true,
        true, true }
    };
  }

  @Test( dataProvider = "scenarios" )
  public void testAll( MetricEvent e, boolean allExpected, boolean exceptionExpected, boolean exceptionsExpected ) {
    Condition all = Conditions.all();
    assertEquals( all.matches( e ), allExpected );
    Condition exception = Conditions.exception();
    assertEquals( exception.matches( e ), exceptionExpected );
    Condition exceptions = Conditions.exception( IllegalStateException.class, UnsupportedOperationException.class );
    assertEquals( exceptions.matches( e ), exceptionsExpected );
  }
}
