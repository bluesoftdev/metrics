/*
 * Copyright 2014 Dana H. P'Simer & BluesSoft Development, LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.bluesoft.util.metrics.core;

import com.bluesoft.util.metrics.core.impl.BasicTimingMetric;
import java.math.BigInteger;
import java.util.concurrent.Callable;
import org.testng.annotations.Test;
import static org.testng.Assert.*;

/**
 *
 * @author danap
 */
public class TimerTest {

  private static final org.slf4j.Logger LOG = org.slf4j.LoggerFactory.getLogger( TimerTest.class );

  @Test
  public void testTimer() throws Exception {
    TimingMetric metric = new BasicTimingMetric( "test" );
    Timer timer = new Timer( metric );
    String result = timer.time( new Callable<String>() {

      @Override
      public String call() throws Exception {
        return "Done";
      }
    } );
    assertEquals( result, "Done" );
    assertEquals( metric.getCount(), BigInteger.ONE );
    final Exception theException = new Exception();
    timer = new Timer();
    timer.setMetric( metric );
    try {
      timer.time( new Callable<String>() {

        @Override
        public String call() throws Exception {
          throw theException;
        }
      } );
      assertEquals( result, "Done" );
    } catch ( Exception ex ) {
      assertSame( ex, theException );
    }
    assertEquals( metric.getCount(), BigInteger.valueOf( 2 ) );
  }
}
