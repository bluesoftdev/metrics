/*
 * Copyright 2014 Dana H. P'Simer & BluesSoft Development, LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.bluesoft.util.metrics.core;

import com.bluesoft.util.metrics.core.impl.BasicMeteringMetric;
import java.math.BigInteger;
import java.util.concurrent.Callable;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertSame;
import org.testng.annotations.Test;

/**
 *
 * @author danap
 */
public class CounterTest {

  private static final org.slf4j.Logger LOG = org.slf4j.LoggerFactory.getLogger( CounterTest.class );

  @Test
  public void testCounter() throws Exception {
    MeteringMetric metric = new BasicMeteringMetric( "test" );
    Counter counter = new Counter( metric );
    String result = counter.count( new Callable<String>() {

      @Override
      public String call() throws Exception {
        Thread.sleep( 1000 );
        return "Done";
      }
    } );
    assertEquals( result, "Done" );
    assertEquals( metric.getCount(), BigInteger.ONE );
    counter = new Counter();
    counter.setMetric( metric );
    final Exception theException = new Exception();
    try {
      counter.count( new Callable<String>() {

        @Override
        public String call() throws Exception {
          Thread.sleep( 1000 );
          throw theException;
        }
      } );
      assertEquals( result, "Done" );
    } catch ( Exception ex ) {
      assertSame( ex, theException );
    }
    assertEquals( metric.getCount(), BigInteger.valueOf( 2 ) );
  }
}
