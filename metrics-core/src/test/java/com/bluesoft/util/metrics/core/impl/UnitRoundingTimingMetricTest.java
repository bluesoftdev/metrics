/*
 * Copyright 2014 Dana H. P'Simer & BluesSoft Development, LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.bluesoft.util.metrics.core.impl;

import com.bluesoft.util.metrics.core.MeteringMetric;
import com.bluesoft.util.metrics.core.Metric;
import com.bluesoft.util.metrics.core.Rate;
import com.bluesoft.util.metrics.core.Timing;
import com.bluesoft.util.metrics.core.TimingMetric;
import com.bluesoft.util.metrics.core.domain.BasicTimingEvent;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import org.testng.annotations.Test;

import static com.bluesoft.endurance.util.collection.Maps.map;
import static com.bluesoft.endurance.util.collection.Pair.pairOf;
import static com.bluesoft.endurance.util.collection.Sets.set;
import static org.easymock.EasyMock.createMock;
import static org.easymock.EasyMock.eq;
import static org.easymock.EasyMock.expect;
import static org.easymock.EasyMock.expectLastCall;
import static org.easymock.EasyMock.replay;
import static org.easymock.EasyMock.reset;
import static org.easymock.EasyMock.same;
import static org.easymock.EasyMock.verify;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertSame;
import static org.testng.Assert.assertTrue;

/**
 *
 * @author Dana P'Simer &lt;danap@bluesoftdev.com&gt;
 */
public class UnitRoundingTimingMetricTest {

  private static final org.slf4j.Logger LOG = org.slf4j.LoggerFactory.getLogger( UnitRoundingTimingMetricTest.class );

  @Test
  public void testCentile() {
    TimingMetric mock = createMock( TimingMetric.class );
    UnitRoundingTimingMetric rounder = new UnitRoundingTimingMetric( TimeUnit.MINUTES, TimeUnit.MILLISECONDS, mock );

    expect( mock.getCentile( eq( BigDecimal.valueOf( 95.0 ) ) ) )
            .andReturn( new TimingImpl( BigDecimal.valueOf( 1500000000L, 0 ), TimeUnit.NANOSECONDS ) );
    replay( mock );
    Timing timing = rounder.getCentile( BigDecimal.valueOf( 95.0 ) );
    assertEquals( timing.getUnit(), TimeUnit.MILLISECONDS );
    assertEquals( timing.getValue(), BigDecimal.valueOf( 1500000000000L, 9 ) );
    verify( mock );

    reset( mock );
    expect( mock.getCentile( eq( BigDecimal.valueOf( 95.0 ) ) ) )
            .andReturn( new TimingImpl( BigDecimal.valueOf( 1500000000L, 0 ), TimeUnit.MILLISECONDS ) );
    replay( mock );
    timing = rounder.getCentile( BigDecimal.valueOf( 95.0 ) );
    assertEquals( timing.getUnit(), TimeUnit.MILLISECONDS );
    assertEquals( timing.getValue(), BigDecimal.valueOf( 1500000000L, 0 ) );
    verify( mock );
  }

  @Test
  public void testGetMean() {
    TimingMetric mock = createMock( TimingMetric.class );
    UnitRoundingTimingMetric rounder = new UnitRoundingTimingMetric( TimeUnit.MINUTES, TimeUnit.MILLISECONDS, mock );
    expect( mock.getMean() ).andReturn( new TimingImpl( BigDecimal.valueOf( 1500000000L, 0 ), TimeUnit.NANOSECONDS ) );
    replay( mock );
    Timing timing = rounder.getMean();
    assertEquals( timing.getUnit(), TimeUnit.MILLISECONDS );
    assertEquals( timing.getValue(), BigDecimal.valueOf( 1500000000000L, 9 ) );
    verify( mock );

    reset( mock );
    expect( mock.getMean() ).andReturn( new TimingImpl( BigDecimal.valueOf( 1500000000L, 0 ), TimeUnit.MILLISECONDS ) );
    replay( mock );
    timing = rounder.getMean();
    assertEquals( timing.getUnit(), TimeUnit.MILLISECONDS );
    assertEquals( timing.getValue(), BigDecimal.valueOf( 1500000000L, 0 ) );
    verify( mock );
  }

  @Test
  public void testGetStandardDeviation() {
    TimingMetric mock = createMock( TimingMetric.class );
    UnitRoundingTimingMetric rounder = new UnitRoundingTimingMetric( TimeUnit.MINUTES, TimeUnit.MILLISECONDS, mock );
    expect( mock.getStandardDeviation() ).andReturn( new TimingImpl( BigDecimal.valueOf( 1500000000L, 0 ), TimeUnit.NANOSECONDS ) );
    replay( mock );
    Timing timing = rounder.getStandardDeviation();
    assertEquals( timing.getUnit(), TimeUnit.MILLISECONDS );
    assertEquals( timing.getValue(), BigDecimal.valueOf( 1500000000000L, 9 ) );
    verify( mock );

    reset( mock );
    expect( mock.getStandardDeviation() ).andReturn( new TimingImpl( BigDecimal.valueOf( 1500000000L, 0 ), TimeUnit.MILLISECONDS ) );
    replay( mock );
    timing = rounder.getStandardDeviation();
    assertEquals( timing.getUnit(), TimeUnit.MILLISECONDS );
    assertEquals( timing.getValue(), BigDecimal.valueOf( 1500000000L, 0 ) );
    verify( mock );
  }

  @Test
  public void testGetTotal() {
    TimingMetric mock = createMock( TimingMetric.class );
    UnitRoundingTimingMetric rounder = new UnitRoundingTimingMetric( TimeUnit.MINUTES, TimeUnit.MILLISECONDS, mock );
    expect( mock.getTotal() ).andReturn( new TimingImpl( BigDecimal.valueOf( 1500000000L, 0 ), TimeUnit.NANOSECONDS ) );
    replay( mock );
    Timing timing = rounder.getTotal();
    assertEquals( timing.getUnit(), TimeUnit.MILLISECONDS );
    assertEquals( timing.getValue(), BigDecimal.valueOf( 1500000000000L, 9 ) );
    verify( mock );

    reset( mock );
    expect( mock.getTotal() ).andReturn( new TimingImpl( BigDecimal.valueOf( 1500000000L, 0 ), TimeUnit.MILLISECONDS ) );
    replay( mock );
    timing = rounder.getTotal();
    assertEquals( timing.getUnit(), TimeUnit.MILLISECONDS );
    assertEquals( timing.getValue(), BigDecimal.valueOf( 1500000000L, 0 ) );
    verify( mock );
  }

  @Test
  public void testGetRate() {
    TimingMetric mock = createMock( TimingMetric.class );
    UnitRoundingTimingMetric rounder = new UnitRoundingTimingMetric( TimeUnit.MINUTES, TimeUnit.MILLISECONDS, mock );
    expect( mock.getRate() ).andReturn( new RateImpl( BigDecimal.valueOf( 15L, 0 ), TimeUnit.SECONDS ) );
    replay( mock );
    Rate rate = rounder.getRate();
    assertEquals( rate.getUnit(), TimeUnit.MINUTES );
    assertEquals( rate.getValue(), BigDecimal.valueOf( 900000000000L, 9 ) );
    verify( mock );

    reset( mock );
    expect( mock.getRate() ).andReturn( new RateImpl( BigDecimal.valueOf( 15L, 0 ), TimeUnit.MINUTES ) );
    replay( mock );
    rate = rounder.getRate();
    assertEquals( rate.getUnit(), TimeUnit.MINUTES );
    assertEquals( rate.getValue(), BigDecimal.valueOf( 15L, 0 ) );
    verify( mock );
  }

  @Test
  public void testGetCount() {
    TimingMetric mock = createMock( TimingMetric.class );
    UnitRoundingTimingMetric rounder = new UnitRoundingTimingMetric( TimeUnit.MINUTES, TimeUnit.MILLISECONDS, mock );
    expect( mock.getCount() ).andReturn( BigInteger.valueOf( 15L ) );
    replay( mock );
    assertEquals( rounder.getCount(), BigInteger.valueOf( 15L ) );
    verify( mock );
  }

  @Test
  public void testGetSnapshot() {
    TimingMetric mock = createMock( TimingMetric.class );
    UnitRoundingTimingMetric rounder = new UnitRoundingTimingMetric( TimeUnit.MINUTES, TimeUnit.MILLISECONDS, mock );
    final BasicTimingMetric snapshot = new BasicTimingMetric( "test" );
    expect( mock.getSnapshot() ).andReturn( snapshot );
    replay( mock );
    assertTrue( rounder.getSnapshot().getClass().isAssignableFrom( UnitRoundingTimingMetric.class ) );
    verify( mock );
  }

  @Test
  public void testGetSnapshotForMetering() {
    MeteringMetric mock = createMock( MeteringMetric.class );
    UnitRoundingMeteringMetric rounder = new UnitRoundingMeteringMetric( TimeUnit.MINUTES, mock );
    final BasicMeteringMetric snapshot = new BasicMeteringMetric( "test" );
    expect( mock.getSnapshot() ).andReturn( snapshot );
    replay( mock );
    assertTrue( rounder.getSnapshot().getClass().isAssignableFrom( UnitRoundingMeteringMetric.class ) );
    verify( mock );
  }

  @Test
  public void testGetChild() {
    TimingMetric mock = createMock( TimingMetric.class );
    UnitRoundingTimingMetric rounder = new UnitRoundingTimingMetric( TimeUnit.MINUTES, TimeUnit.MILLISECONDS, mock );
    final BasicTimingMetric child = new BasicTimingMetric( "test" );
    expect( mock.getChild( eq( "test" ) ) ).andStubReturn( child );
    replay( mock );
    assertTrue( rounder.getChild( "test" ).getClass().isAssignableFrom( UnitRoundingTimingMetric.class ) );
    assertTrue( rounder.getChild( "test" ).getClass().isAssignableFrom( UnitRoundingTimingMetric.class ) );
    verify( mock );
  }

  @Test
  public void testGetChildMeteringWrappingTiming() {
    TimingMetric mock = createMock( TimingMetric.class );
    UnitRoundingMeteringMetric rounder = new UnitRoundingMeteringMetric( TimeUnit.MINUTES, mock );
    final BasicTimingMetric child = new BasicTimingMetric( "test" );
    expect( mock.getChild( eq( "test" ) ) ).andStubReturn( child );
    replay( mock );
    assertTrue( rounder.getChild( "test" ).getClass().isAssignableFrom( UnitRoundingTimingMetric.class ) );
    assertTrue( rounder.getChild( "test" ).getClass().isAssignableFrom( UnitRoundingTimingMetric.class ) );
    assertEquals( ((TimingMetric)rounder.getChild( "test" )).getTimeUnit(), TimeUnit.NANOSECONDS );
    verify( mock );
  }

  @Test
  public void testGetChildMeteringWrappingMetric() {
    TimingMetric mock = createMock( TimingMetric.class );
    UnitRoundingMeteringMetric rounder = new UnitRoundingMeteringMetric( TimeUnit.MINUTES, mock );
    final Metric child = new AbstractMetric( "test" ) {
    };
    expect( mock.getChild( eq( "test" ) ) ).andStubReturn( child );
    replay( mock );
    assertSame( rounder.getChild( "test" ), child );
    verify( mock );
  }

  @Test
  public void testGetChildren() {
    TimingMetric mock = createMock( TimingMetric.class );
    UnitRoundingTimingMetric rounder = new UnitRoundingTimingMetric( TimeUnit.MINUTES, TimeUnit.MILLISECONDS, mock );
    final BasicTimingMetric child1 = new BasicTimingMetric( "child1" );
    final BasicMeteringMetric child2 = new BasicMeteringMetric( "child2" );
    expect( mock.getChildNames() ).andStubReturn( set( "child1", "child2" ) );
    expect( mock.getChild( "child1" ) ).andStubReturn( child1 );
    expect( mock.getChild( "child2" ) ).andStubReturn( child2 );
    replay( mock );
    Map<String, Metric> childMap = rounder.getChildren();
    assertTrue( Map.class.isAssignableFrom( childMap.getClass() ) );
    assertEquals( childMap.size(), 2 );
    assertEquals( childMap.get( "child1" ).getName(), "child1" );
    assertTrue( childMap.get( "child1" ).getClass().isAssignableFrom( UnitRoundingTimingMetric.class ) );
    assertEquals( childMap.get( "child2" ).getName(), "child2" );
    assertTrue( childMap.get( "child2" ).getClass().isAssignableFrom( UnitRoundingMeteringMetric.class ) );
    verify( mock );
  }

  @Test
  public void testGetChildrenNull() {
    TimingMetric mock = createMock( TimingMetric.class );
    UnitRoundingTimingMetric rounder = new UnitRoundingTimingMetric( TimeUnit.MINUTES, TimeUnit.MILLISECONDS, mock );
    expect( mock.getChildNames() ).andStubReturn( null );
    replay( mock );
    Map<String, Metric> childMap = rounder.getChildren();
    assertTrue( Map.class.isAssignableFrom( childMap.getClass() ) );
    assertEquals( childMap.size(), 0 );
    verify( mock );
  }

  @Test
  public void testReset() {
    TimingMetric mock = createMock( TimingMetric.class );
    UnitRoundingTimingMetric rounder = new UnitRoundingTimingMetric( TimeUnit.MINUTES, TimeUnit.MILLISECONDS, mock );
    final BasicTimingMetric snapshot = new BasicTimingMetric( "test" );
    expect( mock.reset() ).andReturn( snapshot );
    replay( mock );
    assertTrue( rounder.reset().getClass().isAssignableFrom( UnitRoundingTimingMetric.class ) );
    verify( mock );
  }

  @Test
  public void testResetForMeteringMetric() {
    MeteringMetric mock = createMock( MeteringMetric.class );
    UnitRoundingMeteringMetric rounder = new UnitRoundingMeteringMetric( TimeUnit.MINUTES, mock );
    final BasicMeteringMetric snapshot = new BasicMeteringMetric( "test" );
    expect( mock.reset() ).andReturn( snapshot );
    replay( mock );
    assertTrue( rounder.reset().getClass().isAssignableFrom( UnitRoundingMeteringMetric.class ) );
    verify( mock );
  }

  @Test
  public void testAddEvent() {
    TimingMetric mock = createMock( TimingMetric.class );
    UnitRoundingTimingMetric rounder = new UnitRoundingTimingMetric( TimeUnit.MINUTES, TimeUnit.MILLISECONDS, mock );
    final BasicTimingEvent event = new BasicTimingEvent( 0L, 15L );
    mock.addEvent( same( event ) );
    expectLastCall().once();
    replay( mock );
    rounder.addEvent( event );
    verify( mock );
  }
}
