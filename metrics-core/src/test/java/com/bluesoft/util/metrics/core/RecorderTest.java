/*
 * Copyright 2014 Dana H. P'Simer & BluesSoft Development, LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.bluesoft.util.metrics.core;

import com.bluesoft.util.metrics.core.impl.BasicTimingMetric;
import com.bluesoft.util.metrics.core.impl.DistributingMetric;
import java.math.BigInteger;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;
import static com.bluesoft.endurance.util.collection.Maps.map;
import static com.bluesoft.endurance.util.collection.Pair.pairOf;

/**
 * Test the {@link Recorder}.
 * <p>
 * @author Dana P'Simer &lt;danap@bluesoftdev.com&gt;
 */
public class RecorderTest {

  private static final org.slf4j.Logger LOG = org.slf4j.LoggerFactory.getLogger( RecorderTest.class );

  @Test
  public void testTryTimer() throws Exception {
    TimingMetric metric = new BasicTimingMetric( "test" );
    TimingMetric exMetric = new BasicTimingMetric( "exTest" );
    DistributingMetric distMetric = new DistributingMetric( "distTest",
                                                            map( pairOf( Conditions.all(), "test" ), pairOf( Conditions.exception(), "exTest" ) ),
                                                            metric, exMetric );

    try ( Recorder t = Recorder.recordFor( distMetric ) ) {
      Thread.sleep( 100L );
    }

    assertEquals( metric.getCount(), BigInteger.ONE );
    assertEquals( exMetric.getCount(), BigInteger.ZERO );

    try ( Recorder t = Recorder.recordFor( distMetric, "foo", "bar", "snafu" ) ) {
      Thread.sleep( 100L );
    }

    assertEquals( metric.getCount(), BigInteger.valueOf( 2 ) );
    assertEquals( exMetric.getCount(), BigInteger.ZERO );
    Recorder t = Recorder.recordFor( distMetric );
    try {
      Thread.sleep( 100L );
      throw new RuntimeException( "test exception" );
    } catch ( RuntimeException ex ) {
      t.setException( ex );
    } finally {
      t.close();
    }
    assertEquals( metric.getCount(), BigInteger.valueOf( 3 ) );
    assertEquals( exMetric.getCount(), BigInteger.ONE );
  }
}
