/*
 * Copyright 2014 Dana H. P'Simer & BluesSoft Development, LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.bluesoft.util.metrics.core.impl;

import com.bluesoft.util.metrics.core.Rate;
import com.bluesoft.util.metrics.core.Timing;
import com.bluesoft.util.metrics.core.TimingEvent;
import com.bluesoft.util.metrics.core.TimingMetric;
import com.bluesoft.util.metrics.core.domain.BasicTimingEvent;
import java.math.BigDecimal;
import org.testng.annotations.Test;

import static com.bluesoft.util.AssertUtils.*;
import static java.math.BigDecimal.ZERO;
import static org.testng.Assert.*;

/**
 *
 * @author danap
 */
public class BasicTimingMetricTest {

  @Test
  public void testCtors() {
    TimingMetric<TimingMetric, TimingEvent> child1 = new BasicTimingMetric<>( "child1", 0 );
    TimingMetric<TimingMetric, TimingEvent> child2 = new BasicTimingMetric<>( "child2", 0 );
    TimingMetric<TimingMetric, TimingEvent> test = new BasicTimingMetric<>( "test", 0, child1, child2 );
    assertEquals( test.getChildNames().size(), 2 );
    assertSame( test.getChild( "child1" ), child1 );
    assertSame( test.getChild( "child2" ), child2 );

    test = new BasicTimingMetric<>( "test", child1, child2 );
    assertEquals( test.getChildNames().size(), 2 );
    assertSame( test.getChild( "child1" ), child1 );
    assertSame( test.getChild( "child2" ), child2 );

    test = test.getSnapshot();
    assertEquals( test.getChildNames().size(), 2 );
    assertNotSame( test.getChild( "child1" ), child1 );
    assertEquals( test.getChild( "child1" ).getName(), "child1" );
    assertNotSame( test.getChild( "child2" ), child2 );
    assertEquals( test.getChild( "child2" ).getName(), "child2" );

    try {
      test.getChild( "child3" );
      fail( "failed to get expected exception: IllegalArgumentException" );
    } catch ( IllegalArgumentException ex ) {
      // Expected
    }
  }

  @Test
  public void testTiming() {
    TimingMetric<TimingMetric, TimingEvent> tm = new BasicTimingMetric<>( "foo", 0 );
    long now = System.currentTimeMillis();
    tm.addEvent( new BasicTimingEvent( now - 5, now ) );
    tm.addEvent( new BasicTimingEvent( now - 10, now ) );
    tm.addEvent( new BasicTimingEvent( now - 3, now ) );
    tm.addEvent( new BasicTimingEvent( now - 4, now ) );
    tm.addEvent( new BasicTimingEvent( now - 5, now ) );
    tm.addEvent( new BasicTimingEvent( now - 2, now ) );

    Timing mean = tm.getMean();
    Timing median = tm.getCentile( new BigDecimal( 50L ) );
    BigDecimal stddev = tm.getStandardDeviation().getValue();
    Timing total = tm.getTotal();
    Rate rate = tm.getRate();

    try {
      tm.getCentile( BigDecimal.valueOf( 105.0 ) );
      fail( "failed to trigger expected exception: IllegalArgumentException" );
    } catch ( IllegalArgumentException ex ) {
      // Expected
    }
    try {
      tm.getCentile( BigDecimal.valueOf( -1.0 ) );
      fail( "failed to trigger expected exception: IllegalArgumentException" );
    } catch ( IllegalArgumentException ex ) {
      // Expected
    }
    assertEquals( tm.getCentile( BigDecimal.valueOf( 100L ) ).getValue(), BigDecimal.valueOf( 10L ) );
    assertNotNull( mean );
    assertNotNull( median );
    assertNotNull( stddev );
    assertNotNull( total );
    assertNotNull( rate );
    assertBigDecimalEquals( mean.getValue(), BigDecimal.valueOf( 48333333333L, 10 ) );
    assertBigDecimalEquals( median.getValue(), new BigDecimal( 4 ) );
    assertBigDecimalEquals( stddev, BigDecimal.valueOf( 2786873996L, 9 ) );
    assertBigDecimalEquals( total.getValue(), new BigDecimal( 29 ) );
    assertBigDecimalEquals( rate.getValue(), new BigDecimal( 6 ) );

    TimingMetric<TimingMetric, TimingEvent> snapshot = tm.getSnapshot();
    mean = snapshot.getMean();
    median = snapshot.getCentile( new BigDecimal( 50L ) );
    stddev = snapshot.getStandardDeviation().getValue();
    total = snapshot.getTotal();
    rate = snapshot.getRate();

    assertNotNull( mean );
    assertNotNull( median );
    assertNotNull( stddev );
    assertNotNull( total );
    assertNotNull( rate );
    assertBigDecimalEquals( mean.getValue(), BigDecimal.valueOf( 48333333333L, 10 ) );
    assertBigDecimalEquals( median.getValue(), new BigDecimal( 4 ) );
    assertBigDecimalEquals( stddev, BigDecimal.valueOf( 2786873996L, 9 ) );
    assertBigDecimalEquals( total.getValue(), new BigDecimal( 29 ) );
    assertBigDecimalEquals( rate.getValue(), new BigDecimal( 6 ) );

    snapshot = tm.reset();
    mean = snapshot.getMean();
    median = snapshot.getCentile( new BigDecimal( 50L ) );
    stddev = snapshot.getStandardDeviation().getValue();
    total = snapshot.getTotal();
    rate = snapshot.getRate();

    assertNotNull( mean );
    assertNotNull( median );
    assertNotNull( stddev );
    assertNotNull( total );
    assertNotNull( rate );
    assertBigDecimalEquals( mean.getValue(), BigDecimal.valueOf( 48333333333L, 10 ) );
    assertBigDecimalEquals( median.getValue(), new BigDecimal( 4 ) );
    assertBigDecimalEquals( stddev, BigDecimal.valueOf( 2786873996L, 9 ) );
    assertBigDecimalEquals( total.getValue(), new BigDecimal( 29 ) );
    assertBigDecimalEquals( rate.getValue(), new BigDecimal( 6 ) );

    mean = tm.getMean();
    median = tm.getCentile( new BigDecimal( 50L ) );
    stddev = tm.getStandardDeviation().getValue();
    total = tm.getTotal();
    rate = tm.getRate();

    assertNull( mean );
    assertNull( median );
    assertEquals( stddev, ZERO );
    assertEquals( total.getValue(), ZERO );
    assertNull( rate );
  }
}
