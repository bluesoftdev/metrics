/*
 * Copyright 2014 Dana H. P'Simer & BluesSoft Development, LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.bluesoft.util.metrics.core;

/**
 * A DTO for providing information about an event that includes timing information.
 * <p>
 * @param <R> the type of event.
 * <p>
 * @author danap
 */
public interface TimingEvent<R> extends MetricEvent<R> {

  /**
   * Get the start time for the event.
   * <p>
   * @return the start time.
   */
  long getStartTime();

  /**
   * Get the end time for the event.
   * <p>
   * @return the end time.
   */
  long getEndTime();

}
