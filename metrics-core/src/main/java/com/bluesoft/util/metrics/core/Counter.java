/*
 * Copyright 2014 Dana H. P'Simer & BluesSoft Development, LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.bluesoft.util.metrics.core;

import com.bluesoft.util.metrics.core.domain.BasicMetricEvent;
import java.util.concurrent.Callable;

/**
 *
 * @author danap
 */
public class Counter {

  private MeteringMetric metric;

  public Counter() {
  }

  public Counter( MeteringMetric metric ) {
    this.metric = metric;
  }

  public void setMetric( MeteringMetric metric ) {
    this.metric = metric;
  }

  public <T> T count( Callable<T> callable ) throws Exception {
    Exception exception = null;
    T result = null;
    try {
      result = callable.call();
      return result;
    } catch ( Exception ex ) {
      exception = ex;
      throw ex;
    } finally {
      metric.addEvent( new BasicMetricEvent( null, result, exception ) );
    }
  }
}
