/*
 * Copyright 2014 Dana H. P'Simer & BluesSoft Development, LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.bluesoft.util.metrics.core.impl;

import com.bluesoft.util.metrics.core.MeteringMetric;
import com.bluesoft.util.metrics.core.Metric;
import com.bluesoft.util.metrics.core.MetricEvent;
import com.bluesoft.util.metrics.core.Rate;
import com.bluesoft.util.metrics.core.TimingMetric;
import java.math.BigInteger;
import java.util.AbstractMap;
import java.util.AbstractSet;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.NoSuchElementException;
import java.util.Set;
import java.util.concurrent.TimeUnit;

/**
 * A metric decorator that rounds the rate information to a different time unit.
 * <p>
 * @param <M> the metric type.
 * @param <E> the event type.
 * <p>
 * @author Dana P'Simer &lt;danap@bluesoftdev.com&gt;
 */
public class UnitRoundingMeteringMetric<M extends MeteringMetric<M, E>, E extends MetricEvent> implements MeteringMetric<M, E> {

  private static final org.slf4j.Logger LOG = org.slf4j.LoggerFactory.getLogger( UnitRoundingMeteringMetric.class );
  private transient Map<String, Metric> children;
  private final M delegate;
  private final TimeUnit rateUnit;

  public UnitRoundingMeteringMetric( TimeUnit rateUnit, M delegate ) {
    this.delegate = delegate;
    this.rateUnit = rateUnit;
  }

  /**
   * Submit an event for this metric to incorporate.
   * <p>
   * @param event the event to incorporate.
   */
  @Override
  public void addEvent( E event ) {
    delegate.addEvent( event );
  }

  /**
   * To allow access to the {@link UnitRoundingTimingMetric}'s implemenation of getTimeUnit.
   * <p/>
   * @return the time unit used by this metric
   */
  protected TimeUnit getTimeUnit() {
    return null;
  }

  /**
   * @return the unit used for rate.
   */
  @Override
  public TimeUnit getRateUnit() {
    return rateUnit;
  }

  /**
   * Get the child for the specified name.
   * <p>
   * @param name the name should be one of the names returned by {@link #getChildNames()}
   * <p>
   * @return the child.
   * <p>
   * @throws IllegalArgumentException when no child with the requested name exists.
   */
  @Override
  public Metric getChild( String name ) throws IllegalArgumentException {
    if ( children == null ) {
      children = new HashMap<>();
    }
    Metric child = children.get( name );
    if ( child == null ) {
      Metric childMetric = delegate.getChild( name );
      if ( childMetric instanceof TimingMetric ) {
        TimeUnit timeUnit = getTimeUnit();
        if ( timeUnit == null ) {
          timeUnit = ((TimingMetric)childMetric).getTimeUnit();
        }
        child = new UnitRoundingTimingMetric<>( getRateUnit(), timeUnit, (TimingMetric)childMetric );
      } else if ( childMetric instanceof MeteringMetric ) {
        child = new UnitRoundingMeteringMetric<>( getRateUnit(), (M)childMetric );
      } else {
        child = childMetric;
      }
      children.put( name, child );
    }
    return child;
  }

  /**
   * Get the set of child metric's names.
   * <p>
   * @return the set of child metric's names.
   */
  @Override
  public Set<String> getChildNames() {
    return delegate.getChildNames();
  }

  /**
   * Get the child metrics contained by this metric.
   * <p>
   * @return the child metrics.
   */
  @Override
  public Map<String, Metric> getChildren() {
    return new AbstractMap<String, Metric>() {

      @Override
      public Set<Entry<String, Metric>> entrySet() {
        return new AbstractSet<Entry<String, Metric>>() {

          @Override
          public Iterator<Entry<String, Metric>> iterator() {
            return new Iterator<Entry<String, Metric>>() {
              Iterator<String> iter = getChildNames() == null ? null : getChildNames().iterator();

              @Override
              public boolean hasNext() {
                if ( iter == null ) {
                  return false;
                }
                return iter.hasNext();
              }

              @Override
              public Entry<String, Metric> next() {
                if ( iter == null ) {
                  throw new NoSuchElementException();
                }
                return new Map.Entry<String, Metric>() {
                  String key = iter.next();

                  @Override
                  public String getKey() {
                    return key;
                  }

                  @Override
                  public Metric getValue() {
                    return getChild( key );
                  }

                  @Override
                  public Metric setValue( Metric value ) {
                    throw new UnsupportedOperationException( "Not supported." );
                  }
                };
              }

              @Override
              public void remove() {
                throw new UnsupportedOperationException( "Not supported." );
              }
            };
          }

          @Override
          public int size() {
            return delegate.getChildNames() == null ? 0 : delegate.getChildNames().size();
          }
        };
      }
    };
  }

  /**
   * The number of event samples that make up the metrics.
   * <p>
   * @return the number of events.
   */
  @Override
  public BigInteger getCount() {
    return delegate.getCount();
  }

  /**
   * Get the name of the metric.
   * <p>
   * @return the name.
   */
  @Override
  public String getName() {
    return delegate.getName();
  }

  /**
   * Returns the rate at which the events are occurring
   * <p>
   * @return the rate events are occurring.
   */
  @Override
  public Rate getRate() {
    Rate r = delegate.getRate();
    if ( r.getUnit() != getRateUnit() ) {
      return r.convertTo( getRateUnit() );
    }
    return r;
  }

  protected void _reset() {
    children = null;
  }

  /**
   * Reset the delegate metric.
   * <p>
   * @return A snapshot of the delegate, wrapped in a rounding metric.
   */
  @Override
  public M reset() {
    _reset();
    return (M)new UnitRoundingMeteringMetric( getRateUnit(), (M)delegate.reset() );
  }

  /**
   * Return a snapshot of the current state.
   * <p>
   * @return the snapshot.
   */
  @Override
  public M getSnapshot() {
    return (M)new UnitRoundingMeteringMetric( getRateUnit(), (M)delegate.getSnapshot() );
  }

}
