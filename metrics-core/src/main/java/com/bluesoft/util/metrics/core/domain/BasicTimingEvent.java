/*
 * Copyright 2014 Dana H. P'Simer & BluesSoft Development, LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.bluesoft.util.metrics.core.domain;

import com.bluesoft.util.metrics.core.TimingEvent;

/**
 * A basic implementation of {@link TimingEvent}.
 * <p>
 * @param <R> the type of the result.
 * <p>
 * @author Dana H. P'Simer &lt;danap@bluesoftdev.com&gt;
 */
public class BasicTimingEvent<R> extends BasicMetricEvent<R> implements TimingEvent<R> {

  private final long startTime;
  private final long endTime;

  public BasicTimingEvent( long startTime, long endTime, Object[] args, R result, Exception exception ) {
    super( args, result, exception );
    this.startTime = startTime;
    this.endTime = endTime;
  }

  public BasicTimingEvent( long startTime, long endTime ) {
    this.startTime = startTime;
    this.endTime = endTime;
  }

  @Override
  public long getStartTime() {
    return startTime;
  }

  @Override
  public long getEndTime() {
    return endTime;
  }
}
