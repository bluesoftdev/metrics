/*
 * Copyright 2014 Dana H. P'Simer & BluesSoft Development, LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.bluesoft.util.metrics.core.impl;

import com.bluesoft.util.metrics.core.Timing;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.concurrent.TimeUnit;

/**
 *
 * @author danap
 */
public class TimingImpl implements Timing {

  private static final int CONVERSION_SCALE = 9;
  private final BigDecimal value;
  private final TimeUnit unit;

  public TimingImpl( BigDecimal value, TimeUnit unit ) {
    this.value = value;
    this.unit = unit;
  }

  @Override
  public BigDecimal getValue() {
    return value;
  }

  @Override
  public TimeUnit getUnit() {
    return unit;
  }

  @Override
  public Timing convertTo( TimeUnit toUnit ) {
    long nanos = unit.toNanos( 1 );
    BigDecimal nanosValue = value.multiply( BigDecimal.valueOf( nanos ) );
    long toNanos = toUnit.toNanos( 1 );
    BigDecimal toUnitValue = nanosValue.divide( BigDecimal.valueOf( toNanos ), CONVERSION_SCALE, RoundingMode.HALF_UP );
    return new TimingImpl( toUnitValue, toUnit );
  }
}
