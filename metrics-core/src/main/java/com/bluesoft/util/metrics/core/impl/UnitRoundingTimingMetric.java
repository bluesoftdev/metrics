/*
 * Copyright 2014 Dana H. P'Simer & BluesSoft Development, LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.bluesoft.util.metrics.core.impl;

import com.bluesoft.util.metrics.core.Timing;
import com.bluesoft.util.metrics.core.TimingEvent;
import com.bluesoft.util.metrics.core.TimingMetric;
import java.math.BigDecimal;
import java.util.concurrent.TimeUnit;

public class UnitRoundingTimingMetric<M extends TimingMetric<M, E>, E extends TimingEvent> extends UnitRoundingMeteringMetric<M, E>
        implements TimingMetric<M, E> {

  private final TimeUnit timeUnit;
  private final TimingMetric delegate;

  public UnitRoundingTimingMetric( TimeUnit rateUnit, TimeUnit timeUnit, M delegate ) {
    super( rateUnit, delegate );
    this.delegate = delegate;
    this.timeUnit = timeUnit;
  }

  @Override
  public TimeUnit getTimeUnit() {
    return timeUnit;
  }

  /**
   * Reset the values to the starting state.
   * <p>
   * @return as snapshot of the current state prior to resetting.
   */
  @Override
  public M reset() {
    _reset();
    return (M)new UnitRoundingTimingMetric( getRateUnit(), timeUnit, (M)delegate.reset() );
  }

  /**
   * Return a snapshot of the current state.
   * <p>
   * @return the snapshot.
   */
  @Override
  public M getSnapshot() {
    return (M)new UnitRoundingTimingMetric( getRateUnit(), timeUnit, (M)delegate.getSnapshot() );
  }

  /**
   * The mean timing for the events.
   * <p>
   * @return the mean, or null of no data has been collected.
   */
  @Override
  public Timing getMean() {
    Timing t = delegate.getMean();
    if ( t.getUnit() != timeUnit ) {
      return t.convertTo( timeUnit );
    }
    return t;
  }

  /**
   * The total time spent on the event.
   * <p>
   * @return the total time.
   */
  @Override
  public Timing getTotal() {
    Timing t = delegate.getTotal();
    if ( t.getUnit() != timeUnit ) {
      return t.convertTo( timeUnit );
    }
    return t;
  }

  /**
   * The standard deviation of the event samples.
   * <p>
   * @return the standard deviation or null if no data has been collected.
   */
  @Override
  public Timing getStandardDeviation() {
    Timing t = delegate.getStandardDeviation();
    if ( t.getUnit() != timeUnit ) {
      return t.convertTo( timeUnit );
    }
    return t;
  }

  /**
   * Calculate the percentile requested.
   * <p>
   * @param centile a number between 1.0 and 100.0 that signifies the percentile requested.
   * <p>
   * @return the percentile requested or null of no data has been collected.
   */
  @Override
  public Timing getCentile( BigDecimal centile ) {
    Timing t = delegate.getCentile( centile );
    if ( t.getUnit() != timeUnit ) {
      return t.convertTo( timeUnit );
    }
    return t;
  }
}
