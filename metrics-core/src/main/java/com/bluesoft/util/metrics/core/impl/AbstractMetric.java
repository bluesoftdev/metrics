/*
 * Copyright 2014 Dana H. P'Simer & BluesSoft Development, LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.bluesoft.util.metrics.core.impl;

import com.bluesoft.util.metrics.core.Clock;
import com.bluesoft.util.metrics.core.Metric;
import com.bluesoft.util.metrics.core.MetricEvent;
import java.math.BigInteger;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 * A base implementation of a {@link Metric} that can have children.
 * <p>
 * @param <T> The type of the interface.
 * @param <E> The type of the Events.
 * <p>
 * @author Dana H. P'Simer &lt;danap@bluesoftdev.com&gt;
 */
public abstract class AbstractMetric<T extends Metric, E extends MetricEvent> implements Metric<T, E> {

  private final String name;
  private final Map<String, Metric> children = new HashMap<>();
  private BigInteger count = BigInteger.ZERO;
  private Clock clock = SystemClock.INSTANCE;

  public AbstractMetric( String name ) {
    this.name = name;
  }

  protected AbstractMetric( String name, Metric... children ) {
    this( name );
    for ( Metric child : children ) {
      this.children.put( child.getName(), child );
    }
  }

  protected AbstractMetric( AbstractMetric metric ) {
    this( metric.getName() );
    clock = metric.getClock();
    for ( Map.Entry<String, Metric> child : (Set<Map.Entry<String, Metric>>)metric.getChildren().entrySet() ) {
      children.put( child.getKey(), child.getValue().getSnapshot() );
    }
    count = metric.getCount();
  }

  protected Clock getClock() {
    return clock;
  }

  public void setClock( Clock clock ) {
    this.clock = clock;
  }

  @Override
  public String getName() {
    return name;
  }

  @Override
  public Set<String> getChildNames() {
    return children.keySet();
  }

  @Override
  public Metric getChild( String name ) throws IllegalArgumentException {
    if ( !children.containsKey( name ) ) {
      throw new IllegalArgumentException( "no such child: " + name );
    }
    return children.get( name );
  }

  @Override
  public Map<String, Metric> getChildren() {
    return Collections.unmodifiableMap( children );
  }

  @Override
  public void addEvent( E event ) {
    if ( count == null ) {
      count = BigInteger.ONE;
    } else {
      count = count.add( BigInteger.ONE );
    }
  }

  @Override
  public BigInteger getCount() {
    return count;
  }

  @Override
  public T reset() {
    count = BigInteger.ZERO;
    return null;
  }

  @Override
  public T getSnapshot() {
    try {
      return (T)this.clone();
    } catch ( CloneNotSupportedException ex ) {
      // Cannot happen
      throw new RuntimeException( ex );
    }
  }
}
