/*
 * Copyright 2014 Dana H. P'Simer & BluesSoft Development, LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.bluesoft.util.metrics.core;

import com.bluesoft.util.metrics.core.domain.BasicTimingEvent;

/**
 * Designed to be used in a try-with-resources block. like so: {@code
 *   Metic metric = ...
 *   ...
 *   try(Recorder r = Recorder.recordFor(metric)) {
 *     ... // Code to time.
 *   }
 * }
 * If you want exceptions to be passed in the event, if one occurs, you cannot use try-with-resources since the close is called before the catch
 * clauses execute. So structure your code like this: {@code
 *   Metric metric = ...
 *   ...
 *   Recorder r = Recorder.recordFor(metric);
 *   try {
 *     ... // Code to time.
 *   } catch(Exception ex) {
 *     r.setException(ex);
 *     ... // process exception.
 *   } finally {
 *     r.close();
 *   }
 * }
 * The time it takes from the constructor of the Recorder to the close of the Recorder is passed as a timing event to the metric.
 * <p>
 * @author Dana P'Simer &lt;danap@bluesoftdev.com&gt;
 */
public class Recorder<R> implements AutoCloseable {

  private static final org.slf4j.Logger LOG = org.slf4j.LoggerFactory.getLogger( Recorder.class );
  private final Metric<? extends Metric, MetricEvent<R>> metric;
  private final long start;
  private final R result;
  private final Object[] args;
  private Exception ex;

  protected Recorder( Metric<? extends Metric, MetricEvent<R>> metric ) {
    this( metric, null );
  }

  protected Recorder( Metric<? extends Metric, MetricEvent<R>> metric, R result, Object... args ) {
    this.metric = metric;
    this.start = System.nanoTime();
    this.result = result;
    this.args = args;
    LOG.debug( "started timing event: {}, {}, {}, {}", metric.getName(), start, result, args );
  }

  /**
   * Used in the catch block to add an exception to the event.
   * <p>
   * @param ex the exception.
   */
  public void setException( Exception ex ) {
    this.ex = ex;
    LOG.debug( "set exception: {}", ex );
  }

  /**
   * Called to stop the timing of the event and sned it to the metric.
   */
  @Override
  public void close() {
    long end = System.nanoTime();
    metric.addEvent( new BasicTimingEvent<>( start, end, args, result, ex ) );
    LOG.debug( "close: {}, {}, {}, {}, {}", start, end, args, result, ex );
  }

  /**
   * Factory method for Recorders.
   * <p>
   * @param <R>    the type of the result.
   * @param metric the metric to send the event to.
   * <p>
   * @return the new recorder.
   */
  public static <R> Recorder<R> recordFor( Metric<? extends Metric, ? extends MetricEvent<R>> metric ) {
    return new Recorder( metric );
  }

  /**
   * Factory method for Recorders.
   * <p>
   * @param <R>    the type of the result.
   * @param metric the metric to send the event to.
   * @param result the result.
   * @param args   the arguments.
   * <p>
   * @return the new recorder.
   */
  public static <R> Recorder<R> recordFor( Metric<? extends Metric, ? extends MetricEvent<R>> metric, R result, Object... args ) {
    return new Recorder( metric, result, args );
  }
}
