/*
 * Copyright 2014 Dana H. P'Simer & BluesSoft Development, LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.bluesoft.util.metrics.core;

import java.math.BigDecimal;
import java.util.concurrent.TimeUnit;

/**
 * A metric that collects timing information.
 * <p>
 * @author danap
 * @param <T> the type of the metric
 * @param <E> the type of the events.
 */
public interface TimingMetric<T extends TimingMetric, E extends TimingEvent> extends MeteringMetric<T, E> {

  /**
   * The mean timing for the events.
   * <p>
   * @return the mean, or null of no data has been collected.
   */
  Timing getMean();

  /**
   * The total time spent on the event.
   * <p>
   * @return the total time.
   */
  Timing getTotal();

  /**
   * The standard deviation of the event samples.
   * <p>
   * @return the standard deviation or null if no data has been collected.
   */
  Timing getStandardDeviation();

  /**
   * Calculate the percentile requested.
   * <p>
   * @param centile a number between 1.0 and 100.0 that signifies the percentile requested.
   * <p>
   * @return the percentile requested or null of no data has been collected.
   */
  Timing getCentile( BigDecimal centile );

  public TimeUnit getTimeUnit();
}
