/*
 * Copyright 2014 Dana H. P'Simer & BluesSoft Development, LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.bluesoft.util.metrics.core;

import java.math.BigInteger;
import java.util.Map;
import java.util.Set;

/**
 * A base interface for all metrics.
 * <p>
 * @author danap
 * @param <T> the type of the metric
 * @param <E> the type of events.
 */
public interface Metric<T extends Metric, E extends MetricEvent> extends Cloneable {

  /**
   * Get the name of the metric.
   * <p>
   * @return the name.
   */
  String getName();

  /**
   * Get the set of child metric's names.
   * <p>
   * @return the set of child metric's names.
   */
  Set<String> getChildNames();

  /**
   * Get the child for the specified name.
   * <p>
   * @param name the name should be one of the names returned by {@link #getChildNames()}
   * <p>
   * @return the child.
   * <p>
   * @throws IllegalArgumentException when no child with the requested name exists.
   */
  Metric getChild( String name ) throws IllegalArgumentException;

  /**
   * Get the child metrics contained by this metric.
   * <p>
   * @return the child metrics.
   */
  Map<String, Metric> getChildren();

  /**
   * Submit an event for this metric to incorporate.
   * <p>
   * @param event the event to incorporate.
   */
  void addEvent( E event );

  /**
   * The number of event samples that make up the metrics.
   * <p>
   * @return the number of events.
   */
  BigInteger getCount();

  /**
   * Reset the values to the starting state.
   * <p>
   * @return as snapshot of the current state prior to resetting.
   */
  T reset();

  /**
   * Return a snapshot of the current state.
   * <p>
   * @return the snapshot.
   */
  T getSnapshot();
}
