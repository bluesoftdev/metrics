/*
 * Copyright 2014 Dana H. P'Simer & BluesSoft Development, LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.bluesoft.util.metrics.core;

/**
 *
 * @author Dana P'Simer &lt;danap@bluesoftdev.com&gt;
 */
public class Conditions {

  private static final org.slf4j.Logger LOG = org.slf4j.LoggerFactory.getLogger( Conditions.class );

  public static final <E extends MetricEvent> Condition<E> not( final Condition<E> condition ) {
    return new Condition<E>() {

      @Override
      public boolean matches( E data ) {
        return !condition.matches( data );
      }

    };
  }

  public static final <E extends MetricEvent> Condition<E> all() {
    return new Condition() {

      @Override
      public boolean matches( MetricEvent data ) {
        return true;
      }
    };
  }

  public static final <E extends MetricEvent> Condition<E> exception() {
    return new Condition() {

      @Override
      public boolean matches( MetricEvent data ) {
        return data.getException() != null;
      }

    };
  }

  public static final <E extends MetricEvent> Condition<E> exception( final Class<? extends Throwable>... exceptions ) {
    return new Condition() {

      @Override
      public boolean matches( MetricEvent data ) {
        Exception ex = data.getException();
        if ( ex == null ) {
          return false;
        }
        Class<? extends Throwable> clazz = ex.getClass();
        for ( Class<? extends Throwable> exClass : exceptions ) {
          if ( exClass.isAssignableFrom( clazz ) ) {
            return true;
          }
        }
        return false;
      }

    };
  }

  private Conditions() {

  }

}
