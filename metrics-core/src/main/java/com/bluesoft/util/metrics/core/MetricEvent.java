/*
 * Copyright 2014 Dana H. P'Simer & BluesSoft Development, LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.bluesoft.util.metrics.core;

/**
 * A DTO for providing information about a generic event.
 * <p>
 * @param <R> The return type of the operation for which metrics are being recorded.
 * <p>
 * @author danap
 */
public interface MetricEvent<R> {

  /**
   * The arguments passed to the operation for which metrics are being recoreded.
   * <p>
   * @return The arguments of the operation if there are any. May be null.
   */
  Object[] getArguments();

  /**
   * The result of the operation for which metrics are being recoreded.
   * <p>
   * @return The result of the operation if there is one. May be null, only valid if {@link #getException()} returns null.
   */
  R getResult();

  /**
   * The exception that occurred, if any.
   * <p>
   * @return The exception or null if none occurred for this event.
   */
  Exception getException();
}
