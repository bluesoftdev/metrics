/*
 * Copyright 2014 Dana H. P'Simer & BluesSoft Development, LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.bluesoft.util.metrics.core.impl;

import com.bluesoft.util.metrics.core.Clock;
import com.bluesoft.util.metrics.core.MeteringMetric;
import com.bluesoft.util.metrics.core.MetricEvent;
import com.bluesoft.util.metrics.core.Rate;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.math.RoundingMode;
import java.util.concurrent.TimeUnit;

/**
 * A basic implementation of a {@link MeteringMetric}
 * <p>
 * @param <T> the type of the metric.
 * @param <E> the type of events.
 * <p>
 * @author danap
 */
public class BasicMeteringMetric<T extends MeteringMetric, E extends MetricEvent> extends AbstractMetric<T, E> implements MeteringMetric<T, E> {

  private static final org.slf4j.Logger LOG = org.slf4j.LoggerFactory.getLogger( BasicMeteringMetric.class );
  private long start = TimeUnit.SECONDS.convert( getClock().getTime(), getClock().getUnit() );
  private Long end = null;

  /**
   * Create a metric with the given name.
   * <p>
   * @param name the name.
   */
  public BasicMeteringMetric( String name ) {
    super( name );
  }

  /**
   * Create a metric with the given name and children.
   * <p>
   * @param name     the name.
   * @param children the children.
   */
  public BasicMeteringMetric( String name, MeteringMetric<T, E>... children ) {
    super( name, children );
  }

  /**
   * Copies the metric and makes the copy a snapshot.
   * <p>
   * @param metric the metric to copy.
   */
  protected BasicMeteringMetric( BasicMeteringMetric metric ) {
    super( metric );
    start = metric.getStart();
    end = TimeUnit.SECONDS.convert( getClock().getTime(), getClock().getUnit() );
  }

  @Override
  public void setClock( Clock clock ) {
    super.setClock( clock );
    // reset the start with the new clock.
    start = TimeUnit.SECONDS.convert( getClock().getTime(), getClock().getUnit() );
  }

  protected long getStart() {
    return start;
  }

  protected Long getEnd() {
    return end;
  }

  protected boolean isSnapshot() {
    return getEnd() != null;
  }

  @Override
  public TimeUnit getRateUnit() {
    return TimeUnit.SECONDS;
  }

  @Override
  public Rate getRate() {
    if ( getCount() == null || getCount() == BigInteger.ZERO ) {
      return null;
    }
    long now = isSnapshot() ? getEnd() : TimeUnit.SECONDS.convert( getClock().getTime(), getClock().getUnit() );
    LOG.debug( "getRate: now = {}", now );
    return new RateImpl( new BigDecimal( getCount() ).divide( BigDecimal.valueOf( now == start ? 1 : now - start ), 10, RoundingMode.HALF_UP ),
                         TimeUnit.SECONDS );
  }

  @Override
  public T reset() {
    T snapshot = getSnapshot();
    super.reset();
    start = TimeUnit.SECONDS.convert( getClock().getTime(), getClock().getUnit() );
    return snapshot;
  }

  @Override
  protected Object clone() throws CloneNotSupportedException {
    return new BasicMeteringMetric( this );
  }
}
