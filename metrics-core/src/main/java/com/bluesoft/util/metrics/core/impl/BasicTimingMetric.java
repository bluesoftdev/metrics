/*
 * Copyright 2014 Dana H. P'Simer & BluesSoft Development, LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.bluesoft.util.metrics.core.impl;

import com.bluesoft.util.BigDecimalUtils;
import com.bluesoft.util.metrics.core.Metric;
import com.bluesoft.util.metrics.core.Timing;
import com.bluesoft.util.metrics.core.TimingEvent;
import com.bluesoft.util.metrics.core.TimingMetric;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.math.MathContext;
import java.math.RoundingMode;
import java.util.List;
import java.util.Map;
import java.util.SortedMap;
import java.util.TreeMap;
import java.util.concurrent.TimeUnit;

/**
 * A Basic implementation of a {@link TimingMetric}. This implementation makes no attempt to syncrhonize mutations to its state, so a synchronizing
 * decorator should be used if multiple threads will be adding events via the {@link #addEvent(com.bluesoft.util.metrics.core.TimingEvent)} method.
 * <p>
 * @param <T> The type of the interface.
 * @param <E> The type of events.
 * <p>
 * @author danap
 */
public class BasicTimingMetric<T extends TimingMetric, E extends TimingEvent> extends BasicMeteringMetric<T, E> implements TimingMetric<T, E> {

  private static final BigDecimal _100 = new BigDecimal( 100 );
  protected BigDecimal mean = BigDecimal.ZERO;
  protected BigDecimal M2 = BigDecimal.ZERO;
  protected BigDecimal total = BigDecimal.ZERO;
  protected SortedMap<BigDecimal, BigInteger> histogram = new TreeMap<>();
  protected int histogramScale = 1;
  protected MathContext mc = new MathContext( 10, RoundingMode.HALF_UP );

  /**
   * Creates a metric with the defaults and the given name.
   * <p>
   * @param name the name of the metric.
   */
  public BasicTimingMetric( String name ) {
    super( name );
  }

  /**
   * Creates a metric with the given name, histogramScale and no children.
   * <p>
   * @param name           the name of the metric
   * @param histogramScale the scale of the histogram.
   */
  public BasicTimingMetric( String name, int histogramScale ) {
    super( name );
    this.histogramScale = histogramScale;
  }

  /**
   * Creates a metric with the given name, children and the default histogramScale.
   * <p>
   * @param name     the name.
   * @param children the children.
   */
  public BasicTimingMetric( String name, TimingMetric<T, E>... children ) {
    super( name, children );
  }

  /**
   * Creates a metric with the given name, children and histogram scale.
   * <p>
   * @param name           the name.
   * @param histogramScale the scale of the histogram.
   * @param children       the children
   */
  public BasicTimingMetric( String name, int histogramScale, TimingMetric<T, E>... children ) {
    super( name, children );
    this.histogramScale = histogramScale;
  }

  /**
   * Creates a copy of the given metric. The new copy will be a Snapshot and should not be fed more events.
   * <p>
   * @param metric the metric to copy.
   */
  protected BasicTimingMetric( BasicTimingMetric<T, E> metric ) {
    super( metric );
    mean = metric.mean;
    M2 = metric.M2;
    total = metric.total;
    histogramScale = metric.histogramScale;
    histogram.putAll( metric.histogram );
    mc = metric.mc;
  }

  @Override
  public TimeUnit getTimeUnit() {
    return TimeUnit.NANOSECONDS;
  }

  @Override
  public Timing getMean() {
    return mean == BigDecimal.ZERO ? null : new TimingImpl( mean, TimeUnit.NANOSECONDS );
  }

  @Override
  public Timing getTotal() {
    return new TimingImpl( total, TimeUnit.NANOSECONDS );
  }

  @Override
  public Timing getStandardDeviation() {
    System.out.println( "M2 = " + M2 );
    BigDecimal v = M2.divide( new BigDecimal( getCount().subtract( BigInteger.ONE ) ), mc );
    System.out.println( "M2/(count-1) = " + v );
    BigDecimal stdDev = BigDecimalUtils.sqrt( v, mc );
    System.out.println( "sqrt(M2/(count-1)) = " + stdDev );
    return new TimingImpl( stdDev, TimeUnit.NANOSECONDS );
  }

  @Override
  public Timing getCentile( BigDecimal centile ) {
    if ( histogram.isEmpty() ) {
      return null;
    }
    if ( centile.compareTo( BigDecimal.ZERO ) < 0 || centile.compareTo( _100 ) > 0 ) {
      throw new IllegalArgumentException( "centile out of range, must be between 0.0 and 100.0 inclusive, was " + centile );
    }
    if ( centile.compareTo( _100 ) == 0 ) {
      return new TimingImpl( histogram.lastKey(), TimeUnit.NANOSECONDS );
    }
    BigInteger centileCount = centile.divide( _100, mc ).multiply( new BigDecimal( getCount() ) ).setScale( 0, RoundingMode.HALF_UP ).
            toBigIntegerExact();
    for ( Map.Entry<BigDecimal, BigInteger> histogramBucket : histogram.entrySet() ) {
      centileCount = centileCount.subtract( histogramBucket.getValue() );
      if ( centileCount.compareTo( BigInteger.ZERO ) <= 0 ) {
        return new TimingImpl( histogramBucket.getKey(), TimeUnit.NANOSECONDS );
      }
    }
    throw new IllegalStateException( "Could not find centile!" );
  }

  @Override
  public void addEvent( E event ) {
    super.addEvent( event );
    TimingEvent tevent = (TimingEvent)event;
    BigDecimal timing = BigDecimal.valueOf( tevent.getEndTime() - tevent.getStartTime() );
    BigDecimal delta = timing.subtract( mean );
    total = total.add( timing );
    mean = mean.add( delta.divide( new BigDecimal( getCount() ), mc ) );
    M2 = M2.add( delta.multiply( timing.subtract( mean ) ) );
    BigDecimal histogramBucket = timing.setScale( histogramScale, RoundingMode.HALF_UP );
    BigInteger count = histogram.get( histogramBucket );
    if ( count == null ) {
      histogram.put( histogramBucket, BigInteger.ONE );
    } else {
      histogram.put( histogramBucket, count.add( BigInteger.ONE ) );
    }
  }

  @Override
  public T reset() {
    T snapshot = getSnapshot();
    super.reset();
    mean = BigDecimal.ZERO;
    M2 = BigDecimal.ZERO;
    total = BigDecimal.ZERO;
    histogram.clear();
    return snapshot;
  }

  @Override
  protected Object clone() throws CloneNotSupportedException {
    return new BasicTimingMetric( this );
  }
}
