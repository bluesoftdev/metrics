/*
 * Copyright 2014 Dana H. P'Simer & BluesSoft Development, LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.bluesoft.util.metrics.core;

import java.math.BigDecimal;
import java.util.concurrent.TimeUnit;

/**
 * Represents a value that is expressed in a time unit or per-time unit. For instance a rate of 50/second would return 50 for value and
 * TimeUnit.SECONDS for unit whereas, a timing of 50ms would return 50 as a value and TimeUnit.MILLISECONDS as a unit.
 * <p>
 * @author danap
 */
public interface TimeUnitValue<T extends TimeUnitValue> {

  /**
   * Get the value.
   * <p>
   * @return the value. May not be null.
   */
  BigDecimal getValue();

  /**
   * Get the unit of the value.
   * <p>
   * @return the unit. May not be null
   */
  TimeUnit getUnit();

  /**
   * Convert this to a new instance with the given timeunit.
   * <p>
   * @param unit the timeunit to convert to
   * <p>
   * @return the converted TimeUnitValue.
   */
  T convertTo( TimeUnit unit );
}
