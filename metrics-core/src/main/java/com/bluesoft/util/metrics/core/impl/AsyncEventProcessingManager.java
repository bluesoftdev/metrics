/*
 * Copyright 2014 Dana H. P'Simer & BluesSoft Development, LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.bluesoft.util.metrics.core.impl;

import com.bluesoft.util.metrics.core.MeteringMetric;
import com.bluesoft.util.metrics.core.Metric;
import com.bluesoft.util.metrics.core.MetricEvent;
import com.bluesoft.util.metrics.core.Timing;
import com.bluesoft.util.metrics.core.TimingEvent;
import com.bluesoft.util.metrics.core.TimingMetric;
import java.lang.invoke.MethodHandle;
import java.lang.invoke.MethodHandles;
import java.lang.invoke.MethodType;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
import java.util.concurrent.Executor;

/**
 * This class will create a {@link Metric} that delegates all methods to another {@link Metric} descendant class except,
 * {@link Metric#addEvent(com.bluesoft.util.metrics.core.MetricEvent)}. For {@link Metric#addEvent(com.bluesoft.util.metrics.core.MetricEvent)} the
 * call is routed to an executor that is injected.
 * <p>
 * @author Dana H. P'Simer &lt;danap@bluesoftdev.com&gt;
 */
public class AsyncEventProcessingManager {

  private static final org.slf4j.Logger LOG = org.slf4j.LoggerFactory.getLogger( AsyncEventProcessingManager.class );

  private Executor executor;

  public AsyncEventProcessingManager() {
  }

  public AsyncEventProcessingManager( Executor executor ) {
    this.executor = executor;
  }

  private class QueuedInvocationHandler<T extends Metric> implements InvocationHandler {

    private final T delegate;
    private final MethodHandle addEvent;

    public QueuedInvocationHandler( T delegate, Class<?> eventClass ) throws IllegalAccessException, NoSuchMethodException {
      this.delegate = delegate;
      addEvent = MethodHandles.lookup().findVirtual( delegate.getClass(), "addEvent", MethodType.methodType( void.class, eventClass ) );
    }

    @Override
    public Object invoke( final Object proxy, final Method method, final Object[] args ) throws Throwable {
      if ( method.getName().equals( "addEvent" ) ) {
        executor.execute( new Runnable() {

          @Override
          public void run() {
            try {
              addEvent.invoke( delegate, args[0] );
            } catch ( Throwable ex ) {
              LOG.error( "error adding event to metric", ex );
            }
          }
        } );
        return null;
      }
      return method.invoke( delegate, args );
    }
  }

  /**
   * Create a decorator that will run addEvent calls through the provided executor.
   * <p>
   * @param <T>           the type of metric being decorated, this should be either {@link Metric} or {@link Timing}.
   * @param <E>           the type of events the metric will handle, either {@link MetricEvent} or {@link TimingEvent}.
   * @param delegate      the implementation to delegate to.
   * @param delegateClass the interface class the delegate implements.
   * @param eventClass    the event interface class that the events will be.
   * <p>
   * @return the decorator.
   */
  public <T extends Metric, E extends MetricEvent> T createQueuedMetric( T delegate, Class<T> delegateClass, Class<E> eventClass ) {
    try {
      return (T)Proxy.
              newProxyInstance( getClass().getClassLoader(), new Class[]{ delegateClass }, new QueuedInvocationHandler( delegate, eventClass ) );
    } catch ( IllegalAccessException | NoSuchMethodException ex ) {
      return null;
    }
  }

  /**
   * Decorates a {@link MeteringMetric}.
   * <p>
   * @param delegate the delegate
   * <p>
   * @return the decorator.
   */
  public MeteringMetric createQueuedMeteringMetric( MeteringMetric delegate ) {
    return createQueuedMetric( delegate, MeteringMetric.class, MetricEvent.class );
  }

  /**
   * Decorates a {@link TimingMetric}.
   * <p>
   * @param delegate the delegate.
   * <p>
   * @return the decorator.
   */
  public TimingMetric createQueuedTimingMetric( TimingMetric delegate ) {
    return createQueuedMetric( delegate, TimingMetric.class, TimingEvent.class );
  }

  /**
   * Set the executor to use.
   * <p>
   * @param executor the executor that will be used to run {@link Metric#addEvent(com.bluesoft.util.metrics.core.MetricEvent) } calls.
   */
  public void setExecutor( Executor executor ) {
    this.executor = executor;
  }

}
