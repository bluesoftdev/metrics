/*
 * Copyright 2014 Dana H. P'Simer & BluesSoft Development, LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.bluesoft.util.metrics.core;

import java.util.concurrent.TimeUnit;

/**
 * A metric that keeps track of the rate events are occurring.
 * <p>
 * @author danap
 * @param <T> the type of the metric.
 * @param <E> the type of the events.
 */
public interface MeteringMetric<T extends MeteringMetric, E extends MetricEvent> extends Metric<T, E> {

  /**
   * Returns the rate at which the events are occurring
   * <p>
   * @return the rate events are occurring.
   */
  Rate getRate();

  /**
   * @return the time unit interval that will be used for reporting rate. e.g. TimeUnit.MINUTES means rate is reported in events/minute.
   */
  TimeUnit getRateUnit();
}
