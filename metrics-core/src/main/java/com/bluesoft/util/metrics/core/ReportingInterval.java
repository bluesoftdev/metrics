/*
 * Copyright 2014 Dana H. P'Simer & BluesSoft Development, LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.bluesoft.util.metrics.core;

import java.util.concurrent.TimeUnit;

/**
 * Metadata about reporting intervals.
 * <p>
 * @author danap
 */
public interface ReportingInterval {

  /**
   * Get the reporting interval's value.
   * <p>
   * @return the value. Will be greater than 0.
   */
  int getIntervalValue();

  /**
   * Get the reporting interval's time unit.
   * <p>
   * @return the time unit. May not be null.
   */
  TimeUnit getIntervalUnit();
}
