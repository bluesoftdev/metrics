/*
 * Copyright 2014 Dana H. P'Simer & BluesSoft Development, LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.bluesoft.util.metrics.core.impl;

import com.bluesoft.util.metrics.core.Condition;
import com.bluesoft.util.metrics.core.Metric;
import com.bluesoft.util.metrics.core.MetricEvent;
import java.util.Map;

/**
 * A metric that distributes events to its children based on conditions provided.
 * <p>
 * @param <M> the type of the metric.
 * @param <E> the type of the metric event.
 * <p>
 * @author danap
 */
public class DistributingMetric<M extends Metric, E extends MetricEvent> extends AbstractMetric<M, E> {

  private Map<Condition<E>, String> conditions;

  protected DistributingMetric( DistributingMetric metric ) {
    super( metric );
    conditions = metric.getConditions();
  }

  public DistributingMetric( String name, Map<Condition<E>, String> conditions, M... children ) {
    super( name, children );
    this.conditions = conditions;
  }

  protected Map<Condition<E>, String> getConditions() {
    return conditions;
  }

  public void setConditions( Map<Condition<E>, String> metrics ) {
    this.conditions = metrics;
  }

  @Override
  public void addEvent( E event ) {
    super.addEvent( event );
    for ( Map.Entry<Condition<E>, String> metric : conditions.entrySet() ) {
      if ( metric.getKey().matches( event ) ) {
        getChild( metric.getValue() ).addEvent( event );
      }
    }
  }
}
