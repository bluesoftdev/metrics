/*
 * Copyright 2014 Dana H. P'Simer & BluesSoft Development, LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.bluesoft.util;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.math.MathContext;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.List;

/**
 * Utilities to do calculations with BigDecimal.
 * <p>
 * @author Dana H. P'Simer
 * @version Java 5, 28 September 2007
 */
public class BigDecimalUtils {

  private static final BigDecimal TWO = BigDecimal.valueOf( 2L );
  private static final double SQRT_10 = 3.162277660168379332;

  /**
   * Returns the correctly rounded square root of a positive BigDecimal.
   * <p>
   * Credit: Frans Lelieveld (http://iteror.org/big/Retro/blog/sec/archive20070915_295396.html)
   * <p>
   * The algorithm for taking the square root of a BigDecimal is most critical for the speed of your application. This method performs the fast Square
   * Root by Coupled Newton Iteration algorithm by Timm Ahrendt, from the book "Pi, unleashed" by Jörg Arndt in a neat loop.
   * <p>
   * @param squarD number to get the root from (called "d" in the book)
   * @param rootMC precision and rounding mode (for the last root "x")
   * <p>
   * @return the root of the argument number
   * <p>
   * @throws ArithmeticException      if the argument number is negative
   * @throws IllegalArgumentException if rootMC has precision 0
   */
  public static BigDecimal sqrt( BigDecimal squarD, MathContext rootMC ) {
    System.out.println( "squarD = " + squarD + ", rootMC = " + rootMC );
    // General number and precision checking
    int sign = squarD.signum();
    if ( sign == -1 ) {
      throw new ArithmeticException( "\nSquare root of a negative number: " + squarD );
    } else if ( sign == 0 ) {
      return squarD.round( rootMC );
    }

    int prec = rootMC.getPrecision();           // the requested precision
    if ( prec == 0 ) {
      throw new IllegalArgumentException( "\nMost roots won't have infinite precision = 0" );
    }

    // Initial precision is that of double numbers 2^63/2 ~ 4E18
    int BITS = 62;                              // 63-1 an even number of number bits
    int nInit = 16;                             // precision seems 16 to 18 digits
    MathContext nMC = new MathContext( 18, RoundingMode.HALF_DOWN );

    // Estimate the square root with the foremost 62 bits of squarD
    BigInteger bi = squarD.unscaledValue();     // bi and scale are a tandem
    int biLen = bi.bitLength();
    int shift = Math.max( 0, biLen - BITS + (biLen % 2 == 0 ? 0 : 1) );   // even shift..
    bi = bi.shiftRight( shift );                  // ..floors to 62 or 63 bit BigInteger

    double root = Math.sqrt( bi.doubleValue() );
    BigDecimal halfBack = new BigDecimal( BigInteger.ONE.shiftLeft( shift / 2 ) );

    int scale = squarD.scale();
    if ( scale % 2 == 1 ) // add half scales of the root to odds..
    {
      root *= SQRT_10;                          // 5 -> 2, -5 -> -3 need half a scale more..
      scale += 1;
    }
    scale /= 2;       // ..where 100 -> 10 shifts the scale

    // Initial x - use double root - multiply by halfBack to unshift - set new scale
    BigDecimal x = new BigDecimal( "" + root, nMC );
    x = x.multiply( halfBack, nMC );                          // x0 ~ sqrt()
    if ( scale != 0 ) {
      x = x.movePointLeft( scale );
    }

    if ( prec < nInit ) // for prec 15 root x0 must surely be OK
    {
      return x.round( rootMC );        // return small prec roots without iterations
    }
    // Initial v - the reciprocal
    BigDecimal v = BigDecimal.ONE.divide( TWO.multiply( x ), nMC );        // v0 = 1/(2*x)

    // Collect iteration precisions beforehand
    List<Integer> nPrecs = new ArrayList<>();

    assert nInit > 3 : "Never ending loop!";                // assume nInit = 16 <= prec

    // Let m be the exact digits precision in an earlier! loop
    for ( int m = prec + 1; m > nInit; m = m / 2 + (m > 100 ? 1 : 2) ) {
      nPrecs.add( m );
    }

    // The loop of "Square Root by Coupled Newton Iteration" for simpletons
    for ( int i = nPrecs.size() - 1; i > -1; i-- ) {
      // Increase precision - next iteration supplies n exact digits
      nMC = new MathContext( nPrecs.get( i ), (i % 2 == 1) ? RoundingMode.HALF_UP : RoundingMode.HALF_DOWN );

      // Next x                                                 // e = d - x^2
      BigDecimal e = squarD.subtract( x.multiply( x, nMC ), nMC );
      if ( i != 0 ) {
        x = x.add( e.multiply( v, nMC ) );                          // x += e*v     ~ sqrt()
      } else {
        x = x.add( e.multiply( v, rootMC ), rootMC );               // root x is ready!
        break;
      }

      // Next v                                                 // g = 1 - 2*x*v
      BigDecimal g = BigDecimal.ONE.subtract( TWO.multiply( x ).multiply( v, nMC ) );

      v = v.add( g.multiply( v, nMC ) );                            // v += g*v     ~ 1/2/sqrt()
    }

    return x;                        // return sqrt(squarD) with precision of rootMC
  }

  /**
   * Compares two BigDecimals for equality.
   * <p>
   * @param c1 the first value.
   * @param c2 the second value.
   * <p>
   * @return true if the two values are equal.
   */
  public static boolean equals( BigDecimal c1, BigDecimal c2 ) {
    if ( c1 == c2 ) {
      return true;
    }
    if ( c1 == null || c2 == null ) {
      throw new IllegalArgumentException( "neither c1 or c2 can be null." );
    }
    return c1.compareTo( c2 ) == 0;
  }
}
